/* eslint-disable */
export default {
    sockets: {
        connect() {
            console.log("Connect Successfully!!");
        },
        disconnect(){
            console.log("Disconnection!!");
          },
        reconnect(){
            console.log("Reconnection!!");
        }
    },
    methods: {
        msgSubscribe() {
            this.sockets.subscribe('messageCreated', (data) => {
                this.getAllMsg();
            });
        },
        msgDeletedSubscribeById() {
            this.sockets.subscribe('aMessageDeleteById', (data) => {
                this.getAllMsg();
            });
        },
        msgReadSubscribe(){
            this.sockets.subscribe('aMessageRead', (data) => {
                this.getAllMsg();
            });
        },
        msgSolvedSubscribe(){
            this.sockets.subscribe('aMessageSolved', (data) => {
                this.getAllMsg();
            });
        },
        createMsgSocket(){
            this.$socket.emit('newMessage', 1,'TestSocket', 'On Vue');
        },
        deleteMsgSocketById(Id) {
            this.$socket.emit('deleteMessageById', Id);
        },
        readMsgSocket(Id){
            this.$socket.emit('readMessage', Id);
        },
        solvedMsgSocket(Id){
            this.$socket.emit('solvedMessage', Id);
        }
    }
}