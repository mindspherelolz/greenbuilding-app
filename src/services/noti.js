/* eslint-disable */
export default {
    data() {
        return {
            msgNum: 0,
            msg: null
        }
    },
    methods: {
        async getAllMsg() {
            try {
                let res = await this.$axios.get(`/backend/v1/noti/`);
                this.msg = res.data;
                this.sortMsgByReadAndSolved();
            }catch(e){
                console.log(e);
            }
        },
        async removeAMsgById() {
            try {
                let res = await this.$axios.delete(`/backend/v1/noti/`);
                return res.data;
            }catch(e){
                console.log(e);
            }
        },
        checkSubject(sub) {
            return  this.msg.find(function(item, index, array){
                    if(item.subject == sub) return true;   
                    return false;        
                });
        },
        sortMsgByReadAndSolved() {
            //sort array by boolean
            var cnt = 0;
            this.msg.sort(function(a, b){
                return a.solved - b.solved || a.read - b.read;
            });
            this.msg.forEach(element => {
                if( element.read == false){
                    cnt++;
                }
            });
            this.msgNum = cnt;
        }
    }
}