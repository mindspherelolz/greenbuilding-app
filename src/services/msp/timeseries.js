/* eslint-disable */
var assetId = 'ed68e42a946441bc8b2a2a54ebb174b2';
export default {
    methods: {
        async getTimeseries(sensor){
            try{
                let res = await this.$axios.get(`/backend/msp/timeseries/${assetId}/${sensor}`);
                var data = res.data;
                console.log(data);
                return data
            }catch(e){
                console.log(e);
            }
        },
        async getAggregates(sensor,start,end,intervalValue,intervalUnit,select){
            try{
                let res = await this.$axios.get(`/backend/msp/timeseries/aggregates/${assetId}/sensor=${sensor}&start=${start}&end=${end}&intervalValue=${intervalValue}&intervalUnit=${intervalUnit}&select=${select}`);
                var data = res.data;
                return data
            }catch(e){
                console.log(e);
            }
        }
    }
}