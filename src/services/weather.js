/* eslint-disable */
export default {
    methods: {
        async getWeather() {
            try{
                let res = await this.$axios.get(`/backend/external/meteor`);
                var data = res.data;
                return data
            }catch(e){
                console.log(e);
            }
        },
        async getWeatherByLocation(location) {
            try{
                let res = await this.$axios.get(`/backend/external/meteor/location=${location}`);
                var data = res.data;
                return data;
            }catch(e){
                console.log(e);
            }
        },
        async getWeatherTaipei() {
            try{
                let res = await this.$axios.get(`/backend/external/meteor/taipei`);
                var data = res.data;
                // console.log(data.location[0].weatherElement);
                return data;
            }catch(e){
                console.log(e);
            }
        },
        // from darksky.net
        async getWeatherSiemens() {
            try{
                let res = await this.$axios.get(`/backend/external/meteor/siemens`);
                var data = res.data;
                console.log(data);
                return data;
            }catch(e){
                console.log(e);
            }
        }
    }
}