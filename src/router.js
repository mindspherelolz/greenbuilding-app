/* eslint-disable */
import Vue from 'vue';
import Router from 'vue-router'
import Home from  './components/pages/Home'
import PlantsDash from './components/pages/PlantsDash'
import Order from './components/pages/Order'
import OrderList from './components/pages/OrderList'

Vue.use(Router);

export default new Router ({
    routes: [
        {
            path:'/',
            name:'Home',
            component: Home
        },
        {
            path:'/plantsdash',
            name:'PlantsDash',
            component: PlantsDash  
        },
        {
            path:'/order',
            name:'Order',
            component: Order
        },
        {
            path:'/orderlist',
            name:'OrderList',
            component: OrderList
        }
    ]
});