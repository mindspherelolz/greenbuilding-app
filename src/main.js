/* eslint-disable */
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import BootstrapVue from 'bootstrap-vue'
import router from './router'

Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.prototype.$axios = axios;

// global methods
import weather from './services/weather'
import socket from './services/socket'
Vue.mixin(weather);
Vue.mixin(socket);

// Vue Socket.io
import VueSocketIO from 'vue-socket.io'
Vue.use(new VueSocketIO({
  debug: true,
  // server site
  connection: '/noti',
  vuex: {
  },
  options: { path: "/backend/socket",transports: ['websocket'] }
}));


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
